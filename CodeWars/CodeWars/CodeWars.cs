﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeWars
{
    public class CodeWars
    {
    public string reverseString(string str)
    {
            char[] arr = str.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
    }

    }
}